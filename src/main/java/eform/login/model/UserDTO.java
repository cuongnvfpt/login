package eform.login.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class UserDTO {

    public String getResettocken() {
        return resettocken;
    }

    public void setResettocken(String resettocken) {
        this.resettocken = resettocken;
    }

    private String resettocken;

}
