package eform.login.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class testController {

    @RequestMapping(path = "/media", method = RequestMethod.GET)
    public String media() {
        return "media";
    }

    @RequestMapping("/home")
    public String welcome(Model m) {
        m.addAttribute("page", 0);
        m.addAttribute("typeS", 3);
        return "home/home";
    }

    @RequestMapping("/login")
    public String loginPage() {
        return "login/login";
    }

    @RequestMapping("/logout-success")
    public String logoutPage() {
        return "logout";
    }


    @RequestMapping(path = "address/page/{typeSort}/{page}", method = RequestMethod.GET)
    public String paging(@PathVariable(name = "typeSort") int typeS, @PathVariable(name = "page") int page, Model m) {
        m.addAttribute("page", page);
        m.addAttribute("typeS", typeS);
        return "home/home";
    }

    @RequestMapping("forms")
    public String createForm() {
        return "form/index";
    }

    @RequestMapping("forms/detail/{id}")
    public String previewForm(Model m, @PathVariable(name = "id") int id) {
        m.addAttribute("id", id);
        return "form/preview";
    }

    @RequestMapping(path = "forms/report")
    public String report() {
        return "report/report";
    }
}
