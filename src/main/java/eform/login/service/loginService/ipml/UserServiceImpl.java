package eform.login.service.loginService.ipml;



import eform.login.entity.loginEntity.User;
import eform.login.repository.loginRepository.UserRepository;
import eform.login.service.loginService.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Optional<User> findByResettocken(String resettocken) {
        return userRepository.findByResettocken(resettocken);
    }


    @Override
    public void save(User user) {
        userRepository.save(user);
    }

}
