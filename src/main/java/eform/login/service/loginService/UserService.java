package eform.login.service.loginService;


import eform.login.entity.loginEntity.User;

import java.util.Optional;

public interface UserService {

    User findByUsername(String username);

    Optional<User> findByEmail(String email);

    Optional<User> findByResettocken(String resettocken);

    public void save(User user);
}
